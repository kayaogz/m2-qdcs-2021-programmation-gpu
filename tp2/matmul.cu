#include <cstdio>  
#include <iostream>
#include "cuda.h"  

#define N 512

// A et C sont stockes par lignes, a savoir A(i, j) = A[i * N + j], C(i, j) = C[i * N + j]
// B est stocke par colonne, a savoir B(i, j) = B[i + j * N]
float *A, *B, *C;

// dA et dC sont stockes par lignes, dC est stocke par colonne
float *dA, *dB, *dC;


// Creer un bloc pour le calcul de chaque element C(i, j), calculer avec 1 thread par bloc
__global__ void multiplyMatrixGPUByBlocks(float *dA, float *dB, float *dC, int n)
{
  // A FAIRE ...
}


// Creer un bloc pour le calcul de blockDim.x elements de C, calculer avec blockDim.x threads par bloc.
// Chaque thread calcule un element de C.
__global__ void multiplyMatrixGPUByBlocksThreads1D(float *dA, float *dB, float *dC, int n)
{
  // A FAIRE ...
}


// Creer un bloc pour le calcul de blockDim.x elements de C, calculer avec blockDim.x threads par bloc.
// Chaque thread calcule un element de C.
// Faire marcher pour N n'est pas multiple de blockDim.x.
__global__ void multiplyMatrixGPUByBlocksThreads1DNonMultiple(float *dA, float *dB, float *dC, int n)
{
  // A FAIRE ...
}


// Creer un bloc pour le calcul de blockDim.x * blockDim.y elements de C, calculer avec blockDim.x * blockDim.y threads par bloc.
// Chaque thread calcule un element de C.
__global__ void multiplyMatrixGPUByBlocksThreads2D(float *dA, float *dB, float *dC, int n)
{
  // A FAIRE ...
}


// Creer un bloc pour le calcul de blockDim.x * blockDim.y elements de C, calculer avec blockDim.x * blockDim.y threads par bloc.
// Chaque thread calcule un element de C.
// Faire marcher pour N n'est pas multiple de ni blockDim.x ni blockDim.y.
__global__ void multiplyMatrixGPUByBlocksThreads2DNonMultiple(float *dA, float *dB, float *dC, int n)
{
  // A FAIRE ...
}


// Code reference de CPU pour effectuer la multiplication de matrices C = AB
void multiplyMatrixCPU()
{
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      C[i * N + j] = 0.0f;
      for (int k = 0; k < N; k++) {
        C[i * N + j] += A[i * N + k] * B[k + j * N];
      }
    }
  }
}

void verifyResults()
{
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      float c = 0.0f;
      for (int k = 0; k < N; k++) {
        c += A[i * N + k] * B[k + j * N];
      }
      if (std::abs(C[i * N + j] - c) > 1e-6) {
        std::cout << "Multiplication is incorrect for the element C[" << i << "][" << j << "]" << std::endl;
        return;
      }
    }
  }
  std::cout << "Multiplication is correct!" << std::endl;
}

int main(int argc, char **argv)
{
  // Initialisation
  A = (float *)malloc(N * N * sizeof(A[0]));
  B = (float *)malloc(N * N * sizeof(B[0]));
  C = (float *)malloc(N * N * sizeof(C[0]));
  for (int j = 0; j < N; j++) { 
    for (int i = 0; i < N; i++) { 
      A[i + j * N] = i + j; // A(i, j) = 0
      B[i + j * N] = 1.0f; // B(j, i) = 0
    }
  }

  // Allouer dA et dB, puis copier les tableaux A et B vers le GPU
  // A FAIRE ...

  // Appeler chaque kernel GPU de maniere appropriee pour multiplier les matrices A et B
  // Mesurer et afficher le temps d'execution et la performance (en GFlops/s) de chaque kernel, sans compter le temps de transfert.
  // A FAIRE ...
  dim3 dimGrid;
  dim3 dimBlock;
  // multiplyMatrixGPUByBlocks<<<..., ...>>>(N);
  // multiplyMatrixGPUByBlocksThreads1D<<<..., ...>>>(N);
  // multiplyMatrixGPUByBlocksThreads1DNonMultiple<<<..., ...>>>(N);
  // multiplyMatrixGPUByBlocksThreads2D<<<..., ...>>>(N);
  // multiplyMatrixGPUByBlocksThreads2DNonMultiple<<<..., ...>>>(N);


  // Recopier le tableau dC vers le CPU
  // A FAIRE ...

  // Verifier les resultats
  // multiplyMatrixCPU();
  verifyResults();

  // Desallouer A, B, C
  free(A); free(B); free(C);

  // Desallouer dA, dB, dC
  // A FAIRE ...

  return 0;
}
