/** Le but de cet exercice est d'appliquer un filtre de lissage B[i] = (A[i - 1] + A[i] + A[i + 1]) / 3.0
  * Le code CPU est fourni ci-dessous, le code GPU est a realiser.
  * Realiser un kernel tel que chaque thread calcul un element du tableau B.
  */

#include <cstdio>  
#include <iostream>
#include "cuda.h"  

#define N 1024
#define BLOCK_SIZE_X 32

float *A, *B, *dA, *dB;

__global__ void filtreGPU(float *dA, float *dB, int n)
{
	float res;

  // Allouer de la memoire partagee et charger le bloc correspondant de dA la dedans
	__shared__ float shA[BLOCK_SIZE_X];

  int idx = threadIdx.x + (blockDim.x-2) * blockIdx.x;

	shA[threadIdx.x] = dA[idx];

  // Syncroniser pour que le bloc entier soit charge
  __syncthreads();

  // Effectuer le calcul sur B
	if (threadIdx.x > 0 && threadIdx.x < blockDim.x - 1) {
		res = (shA[threadIdx.x - 1] + shA[threadIdx.x] + shA[threadIdx.x + 1]) / 3.0;
		dB[idx] = res;
	}
}

void filtreCPU(float *A, float *B, int n)
{
  for (int i = 1; i < n - 1; i++) {
    B[i] = (A[i - 1] + A[i] + A[i + 1]) / 3.0;
  }
}

void verify(float *A, float *B, int n)
{
	float b;
  for (int i = 1; i < n - 1; i++) {
    b = (A[i - 1] + A[i] + A[i + 1]) / 3.0;
		if (std::abs(B[i] - b) > 1e-6) {
			printf("Valeur incorrecte : B[%d]=%f et b=%f\n", i, B[i], b);
			return;
		}
  }
	printf("Correct.\n");
}

int main()
{
  // Allouer A et B, puis initialiser A
  A = (float *) malloc (N * sizeof(A[0]));
  B = (float *) malloc (N * sizeof(B[0]));
  for (int i = 0; i < N; i++) { A[i] = i * i; }

  // Allouer la memoire pour dA et dB, puis copier A la-dedans
	cudaMalloc(&dA, N * sizeof(A[0]));
	cudaMalloc(&dB, N * sizeof(B[0]));
	cudaMemcpy(dA, A, N * sizeof(A[0]), cudaMemcpyHostToDevice);
	cudaMemcpy(dB, B, N * sizeof(B[0]), cudaMemcpyHostToDevice);

  // Lancer le kernel avec un certain nombre de blocs et threads
	dim3 Db, Dg;
	Db.x = BLOCK_SIZE_X;
	Db.y = 1;
	Db.z = 1;
	Dg.x = (N-1)/(BLOCK_SIZE_X-2) + 1;
	Dg.y = 1;
	Dg.z = 1;

	filtreGPU<<<Dg, Db>>>(dA, dB, N);

  // Recuperer dB dans B, puis afficher pour verification
	cudaMemcpy(B, dB, N * sizeof(B[0]), cudaMemcpyDeviceToHost);

	verify(A, B, N);

  // Desallouer A et B
  free(A);
  free(B);

  // Desallouer dA et dB
	cudaFree(dA);
	cudaFree(dB);

  return 0;
}
