/** Le but de cet exercice est d'appliquer un filtre de lissage B[i] = (A[i - 1] + A[i] + A[i + 1]) / 3.0
  * Le code CPU est fourni ci-dessous, le code GPU est a realiser.
  * Realiser un kernel tel que chaque thread calcul un element du tableau B.
  */

#include <cstdio>  
#include <iostream>
#include "cuda.h"  

#define N 1024

float *A, *B, *dA, *dB;

__global__ void filtreGPU(float *dA, float *dB, int n)
{
  // Allouer de la memoire partagee et charger le bloc correspondant de dA la dedans
  // A FAIRE ...
  
  // Syncroniser pour que le bloc entier soit charge
  // A FAIRE ...

  // Effectuer le calcul sur B
  // A FAIRE ...
}

void filtreCPU(float *A, float *B, int n)
{
  for (int i = 1; i < n - 1; i++) {
    B[i] = (A[i - 1] + A[i] + A[i + 1]) / 3.0;
  }
}

int main()
{
  // Allouer A et B, puis initialiser A
  A = (float *) malloc (N * sizeof(A[0]));
  B = (float *) malloc (N * sizeof(B[0]));
  for (int i = 0; i < N; i++) { A[i] = i * i; }

  // Allouer la memoire pour dA et dB, puis copier A la-dedans
  // A FAIRE ...

  // Lancer le kernel avec un certain nombre de blocs et threads
  // A FAIRE ...

  // Recuperer dB dans B, puis afficher pour verification
  // A FAIRE ...

  // Desallouer A et B
  free(A);
  free(B);

  // Desallouer dA et dB
  // A FAIRE ...

  return 0;
}
